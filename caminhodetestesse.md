# CAMINHOS DE TESTE SE

 **Todos os testes deverão ser realizados no ambiente de [produção](https://se.ftd.com.br/login) usando as credenciais da escola *desenvolvedores*** 

___

## Caminho do Aluno

### Login

**Dado que** sou um usuário cadastrado  
**E** devidamente ensalado  
**Quando** acesso a tela de login  
**E** insiro as credenciais corretas  
**Então** devo ser capaz de acessar a home do sistema  

___

### Home page

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a home page  
**Então** devo ser capaz de ver e acessar os destaques dinâmicos a seguir: *Atividade online de hoje*, *Aulas de hoje* e *Eventos e feriados*  
**E** em Atividades Online de Hoje deve mostrar a lista das atividades, desde que vinculadas a
turma que está selecionada no Header e marcadas para iniciar no dia do acesso  
**Ou** em caso de não haver Atividades pro dia, deverá aparecer apenas um botão Ir para
[Atividades](https://se.ftd.com.br/atividades)  
**E** em Aulas de Hoje deve apresentar uma lista com todas aulas e/ou eventos, desde que
vinculadas a turma que está selecionada no Header e marcados para o dia do acesso  
**Ou**  em caso de não haver Aulas/Eventos pro dia, deverá aparecer apenas um botão [Ir para
Aulas](https://se.ftd.com.br/aula)   
**E** em Eventos e Feriados devem ser apresentadas datas especiais do mês corrente  

___

### Menus  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso qualquer página do sistema com exceção de [*Leitores*](http://leitores.ftd.com.br/)  
**Então** devo ser capaz de acessar o menu do lado esquerdo da tela com as opções: *Início*, *Publicações e recursos*, *Aula*, *Atividades*, *Central da turma*, *Arquivos*, *Acervo de links* e *Leitores*  

___  

### Publicações e recursos  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**Então** devo ser capaz de acessar as abas: *Publicações* e *Recursos FTD* além de seus respectivos conteúdos  
**E** também devo ser capaz de usar, em ambas as abas, a busca de itens por nome, filtro de disciplina, ordem alfabética, favoritos e controle de itens exibidos por página  

#### Vincular publicações  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Publicações*  
**Então** devo ser capaz de visualizar e selecionar múltiplas publicações  
**E** vinculá-las ao leitor clicando no botão *vincular ao Leitor FTD*  

#### Filtrar recursos  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Recursos FTD*  
**Então** devo ser capaz de visualizar e filtrar os OEDs listados por tipo, sendo todos: áudio, vídeo e multimídia (peças
interativas em html5)  
**E** também devo ser capaz de favoritar múltiplos OEDs  

___  

### Aula  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**Então** devo ser capaz de acessar um calendário de visualização mensal ou semanal com todas as aulas, aulas extras e eventos associados ao período exibido e à turma selecionada no header  

#### Visualizando uma aula  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula exibida no calendário  
**Então** devo ser capaz de acessar todo o conteúdo inserido pelo Professor tal como: recursos da FTD (OED’s), descrições com objetivo da aula,conteúdos, materiais, avaliação, habilidades, Para saber + e/ou desenvolvimento  
**E** também devo ser capaz de ver o status no qual se encontram as aulas: : Aula publicada (verde), Aula não publicada (amarelo), Aula Extra (Um pequeno “E” no canto superior esquerdo do evento), Evento da Escola (azul), Evento da Turma (rosa)  

___

### Atividades  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ser capaz de visualizar e acessar todas as atividades já aplicadas para a turma na qual estou ensalado e selecionado no momento  
**E** as miniaturas de cada atividade devem mostrar informações como: *Status*, *Data de criação* e *Período de disponibilidade*  
**Quando** o *Status* da atividade estiver marcado como *Tempo esgotado*  
**Então** não devo ser capaz de interagir com a atividade  
**Quando** o *Status* da atividade estiver marcado como *Disponível*  
**Então** devo ser capaz de interagir com a atividade e responder as questões  
**Quando** o *Status* da atividade estiver marcado como *Entregue e corrigida*  
**Então** devo ser capaz de abrir a atividade e conferir meu desempenho  

#### Completando atividades  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** acesso uma atividade marcada como disponível  
**Então** um modal apresentará a Atividade para que seja respondida  
**E** No modal deverão ser exibidas as seguintes informações: título, disciplina, criador da atividade, descrição, número de questões, botão *Iniciar Atividades*  
**E** ao iniciar, teremos as questões na sequência definida pelo professor
**E** os botões *Entregar Atividade* e *Sair da Atividade*  
**E** ao tentar entregar a atividade sem nenhuma resposta deverá ser questionado se deseja continuar a ação ou cancelar  

___

### Central da Turma  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Central da Turma*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ter acesso apenas aos meus dados além disso posso ver todos os Professores da turma a qual estou contextualizado  

___

### Arquivos    

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Arquivos*](https://se.ftd.com.br/arquivos) no menu  
**Então** devo ser capaz de visualizar ou baixar arquivos compartilhados por professores ou coordenadores comigo ou com a minha turma    
**E** também devo ser capaz de buscar esses arquivos por nome  
**E** ordená-los por ordem alfabética ou data de compartilhamento  
**E** devo ser capaz de visualizar ,logo abaixo de um arquivo, a data em que foi compartilhado e qual usuário compartilhou  

___

### Acervo de Links  

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Acervo de links*](https://se.ftd.com.br/links) no menu  
**Então** terei acesso a um acervo de links, com curadoria da FTD, sobre assuntos ligados a pedagogia, conhecimentos acadêmicos e conhecimentos gerais  
**E** ao acessar um desses itens, o sistema deve apresentar o ambiente do encurtador de links da FTD que me assegurará que o endereço selecionado continua ativo e acessível  

___

### Leitores FTD

**Dado que** sou um aluno logado com sucesso  
**Quando** acesso a opção [*Leitores*](http://leitores.ftd.com.br/) no menu  
**Então** o sistema deverá abrir uma nova aba no navegador dando acesso a uma página simples e informativa sobre como baixar o Leitor FTD para
as mais diferentes plataformas  

___

### Logout

**Dado que** sou um aluno logado com sucesso  
**Quando** estiver em qualquer seção ou módulo do portal  
**Então** devo ser capaz de deslogar clicando no ícone de perfil, e depois clicando em *Sair*  
**E** o sistema deve apresentar novamente a tela de Login  

___

## Caminho do Professor  

### Login

**Dado que** sou um usuário cadastrado  
**E** devidamente ensalado  
**Quando** acesso a tela de login  
**E** insiro as credenciais corretas  
**Então** devo ser capaz de acessar a home do sistema  

___

### Home page

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a home page  
**Então** devo ser capaz de ver e acessar os destaques dinâmicos a seguir: *Atividade online de hoje*, *Aulas de hoje* e *Eventos e feriados*  
**E** em Atividades Online de Hoje deve mostrar a lista das atividades, desde que vinculadas a turma que está selecionada no Header e marcadas para iniciar no dia do acesso  
**Ou** em caso de não haver Atividades pro dia, deverá aparecer apenas um botão [Ir para Atividades](https://se.ftd.com.br/atividades)  
**E** em Aulas de Hoje deve apresentar uma lista com todas aulas e/ou eventos, desde que vinculadas a turma que está selecionada no Header e marcados para o dia do acesso  
**Ou**  em caso de não haver Aulas/Eventos pro dia, deverá aparecer apenas um botão [Ir para Aulas](https://se.ftd.com.br/aula)   
**E** em Eventos e Feriados devem ser apresentadas datas especiais do mês corrente  

___

### Menus  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso qualquer página do sistema com exceção de [*Leitores*](http://leitores.ftd.com.br/)  
**Então** devo ser capaz de acessar o menu do lado esquerdo da tela com as opções: *Início*, *Publicações e recursos*, *Planejador de Aulas*, *Aula*, *Atividades*, *Central da turma*, *Arquivos*, *Acervo de links*, *Formação Continuada* e *Leitores*  

___  

### Publicações e recursos  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**Então** devo ser capaz de acessar as abas: *Publicações* e *Recursos FTD* além de seus respectivos conteúdos  
**E** também devo ser capaz de usar, em ambas as abas, a busca de itens por nome, filtro de disciplina, ordem alfabética, favoritos e controle de itens exibidos por página  

#### Vincular publicações  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Publicações*  
**Então** devo ser capaz de visualizar e selecionar múltiplas publicações  
**E** vinculá-las ao leitor clicando no botão *vincular ao Leitor FTD*  

#### Filtrar recursos  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Recursos FTD*  
**Então** devo ser capaz de visualizar e filtrar os OEDs listados por tipo, sendo todos: áudio, vídeo e multimídia (peças
interativas em html5)  
**E** também devo ser capaz de favoritar múltiplos OEDs  

___

### Planejador de Aulas  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Planejador de aulas*](https://se.ftd.com.br/planejador) no menu  
**Então** devo ser capaz de visualizar todos os mapas didáticos já criados para o ano letivo e disciplina destacados no header  

#### Criando mapas didáticos  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Planejador de aulas*](https://se.ftd.com.br/planejador) no menu  
**E** clico no botão *Criar novo mapa*  
**Então** o sistema deve apresentar uma tela onde é possível inserir texto para nomear o novo mapa  
**Quando** um nome de pelo menos 3 caracteres é inserido  
**Então** o sistema deve habilitar os botões *Avançar* e *Salvar rascunho*  
**Quando** clico em *Salvar rascunho*  
**Então** o sistema deve notificar o salvamento  
**E** apresentar novamente a tela onde estão os mapas criados  
**Quando** clico em *Avançar*  
**Então** o sistema deve apresentar uma tela de seleção de sequências didáticas, onde posso filtrar e selecionar sequências didáticas já criadas  
**E** criar novas sequências didáticas  
**Quando** seleciono ao menos uma sequência didática para compor o mapa  
**Então** o sistema deve habilitar o botão *Finalizar*  
**Quando** clicar no botão *Finalizar*  
**Então** o sistema deve apresentar um modal notificando o salvamento  
**E** perguntando se desejo aplicar o mapa  
**Quando** decido não aplicar o mapa  
**Então** o sistema apresenta um modal notificando a finalização bem-sucedida do mapa

#### Criando sequências didáticas  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Planejador de aulas*](https://se.ftd.com.br/planejador) no menu  
**E** clico no botão *Avançar* após selecionar *Criar novo mapa*  
**Ou** seleciono a opção *Editar* nas reticências ao lado de um mapa marcado como rascunho  
**Então** o sistema deve apresentar uma tela de seleção de sequências didáticas com o botão *Criar nova sequência didática*  
**Quando** clico no botão *Criar nova sequência didática*  
**Então** o sistema deve apresentar um modal com os campos *Nome da nova sequência didática* e *Quantidade de aulas*  
**Quando** o campo *Nome da nova sequência didática* contiver ao menos 3 caracteres válidos  
**E** o campo *Quantidade de aulas* contiver ao menos um número  
**Então** o sistema deve habilitar o botão *Criar* presente no modal  
**Quando** clicar no botão *Criar*  
**Então** o sistema deve apresentar a *tela padrão¹* de criação de sequência didática  
**Quando** clicar no botão *Incluir arquivos*  
**Então** o sistema deve apresentar um modal com as abas *Arquivos* e *Upload*   
**Quando** seleciono a aba *Arquivos*   
**Então** devo ser capaz de visualizar os arquivos já anexados à sequência, bem como o nome do proprietário do arquivo e o último acesso ao arquivo  
**Quando** seleciono a aba *Upload*  
**Então** devo ser capaz de escolher arquivos do computador local para upload  
**Quando** arrasto um ou mais arquivos de qualquer local do computador para o modal  
**Ou** clico no botão *Selecionar arquivos do computador*  
**Então** o sistema deve mostrar o nome dos arquivos selecionados ou arrastados no modal  
**Quando** pelo menos um arquivo estiver presente no modal  
**Então** o sistema deve habilitar o botão *Inserir*  
**Quando** clico no botão *Inserir*  
**Então** os arquivos presentes no modal devem ser anexados à sequência didática  
**Quando** clico nas reticências ao lado de um arquivo anexado  
**Então** devo ser capaz de visualizar, excluir, baixar ou renomear o arquivo  
**Quando** clico na aba *Recursos*  
**Então** devo ser capaz de visualizar e acessar recursos já anexados à sequência didática, além do botão *Incluir recurso*  
**Quando** clico no botão *Incluir recurso*  
**Então** o sistema deve apresentar a listagem de recursos disponíveis além de um campo de busca por nome  
**Quando** clico no botão de seleção de um ou mais recursos  
**E** clico no botão *Incluir*  
**Então** os recursos selecionados devem ser anexados à sequência didática  
**Quando** clico em qualquer outra aba da criação de sequência  
**Então** devo ser capaz de visualizar o conteúdo já incluso naquela aba, além do botão *Editar*  
**Quando** clico no botão *Editar*  
**Então** o sistema deve apresentar um campo de edição de texto onde é possível incluir textos e inserir imagens, além do botão *Salvar*  
**Quando** clico no botão salvar  
**Então** devo ser capaz de salvar as alterações nas entradas daquela aba  
**Quando** clico no botão *Voltar* no canto superior direito da tela  
**Então** as alterações na sequência didática devem ser salvas  
**E** o sistema deve voltar para a tela de criação e edição de mapas didáticos  

#### Aplicando mapas didáticos

**Dado que** sou um professor logado com sucesso  
**E** não tenho nenhum mapa didático aplicado às minhas aulas extras  
**Quando** clico nas reticências ao lado de qualquer mapa finalizado  
**E** seleciono a opção *Aplicar*  
**Então** devo ser capaz de aplicar um mapa a essas aulas  
**E** o sistema deve notificar a aplicação bem-sucedida do mapa com uma mensagem no canto inferior direito da tela  

#### Substituindo mapas aplicados  

**Dado que** sou um professor logado com sucesso  
**E** já tenho um mapa didático aplicado às minhas aulas extras  
**Quando** clico nas reticências ao lado de um mapa aplicado  
**E** seleciono a opção *retirar*  
**Então** devo ser capaz de desassociar o mapa em questão das aulas as quais está vinculado  
**E** devo ser capaz de aplicá-lo novamente  
**Ou** aplicar outro mapa  

___  

### Aula  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**Então** devo ser capaz de acessar um calendário de visualização mensal ou semanal com todas as aulas, aulas extras e eventos associados ao período exibido e à turma selecionada no header  

#### Visualizando uma aula  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula exibida no calendário  
**Então** devo ser capaz de acessar todo o conteúdo inserido pelo Professor tal como: recursos da FTD (OED’s), descrições com objetivo da aula,conteúdos, materiais, avaliação, habilidades, Para saber + e/ou desenvolvimento  
**E** também devo ser capaz de ver o status no qual se encontram as aulas: : Aula publicada (verde), Aula não publicada (amarelo), Aula Extra (Um pequeno “E” no canto superior esquerdo do evento), Evento da Escola (azul), Evento da Turma (rosa)  

#### Criando eventos ou aulas extras  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer dia exibido no calendário  
**Então** devo ser capaz de configurar as informações do evento como data, hora de início e fim, além de escolher entre os tipos *Aula Extra* ou *Evento*  
**Quando** a opção escolhida for *Evento*  
**Então** o sistema deve solicitar, além das informações mencionadas, título e descrição  
**Quando** a opção escolhida for *Aula Extra*  
**Então** devo indicar, além das informações mencionadas, a sequência didática que desejo associar a essa aula  

#### Publicando, encerrando ou excluindo aulas extras ou eventos

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula não publicada exibida no calendário  
**Então** devo ser capaz de publicar a aula clicando em *Publicar Aula*  
**E** o status dessa aula deve mudar de aula não publicada para aula publicada  
**Quando** clico em qualquer aula publicada exibida no calendário  
**Então** devo ser capaz de encerrar a aula clicando em *Encerrar Aula*  
**Quando** clico em qualquer evento exibido no calendário  
**Então** devo ser capaz de excluí-lo clicando no símbolo de lixeira  

___

### Atividades  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** clico no botão criar atividade  
**Então** devo ser capaz de criar uma atividade, optando por online ou impressa  
**E** indicando um nome e descrição para a mesma  
**E** adiante, clicando em avançar devo ser capaz de selecionar questões curadas pela FTD ou de autoria própria, para que façam parte da atividade  
**E** as questões selecionadas devem ser exibidas num "carrinho de compra" ao lado direito da tela, podendo ser rearranjadas ou removidas  
**E** também devo ser capaz de determinar notas para cada questão selecionada.  

#### Criando questões próprias  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** estiver selecionando questões para compor uma atividade  
**Então** devo ser capaz de criar questões próprias clicando no ícone + no fim da listagem no bloco "Insira questões do seu acervo pessoal"  
**E** no modal que se abrir, devo indicar informações de classificação como tags, nível de ensino e disciplina além de determinar o tipo da questão  
**E** desenvolver um enunciado, usando texto e/ou imagem  
**E** desenvolver uma resolução, usando texto e /ou imagem  
**E** essa questão deve estar disponível apenas no meu acervo , apenas para acesso pessoal, junto a lista de questões curadas pela FTD  

#### Aplicando atividades  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** acesso uma atividade marcada como *Pronta para Aplicar*  
**Então** devo ser capaz de definir uma turma para qual quero aplicar a atividade  
**E** escolher o horário de início e fim da mesma assim como data  
**E** também devo ser capaz de escolher se a prova deve ter tempo controlado e se deve ser aplicada para toda a turma  
**Quando** o tempo for controlado  
**Então** devo ser capaz de definir quanto tempo os alunos terão para realizar a atividade  
**Quando** a atividade for aplicada apenas para alguns alunos  
**Então** devo ser capaz de selecionar os alunos para os quais a atividade estará disponível  
**Ou** na lista de atividades criadas  
**Quando** acesso uma atividade marcada como *Rascunho*  
**Então** devo ser capaz de editar as questões e nota da atividade  
**E** aplicá-la seguindo o processo descrito para atividades marcadas como *Pronta para aplicar*  

#### Visualizando atividades  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ser capaz de visualizar atividades em divididas em três abas: *Minhas Atividades*, *Atividades Aplicadas Online*, *Atividades aplicadas impressas*  
**E** para cada uma das abas, com exceção de *Atividades Aplicadas Impressas*, deverão constar dois status possíveis  
**Quando** a atividade estiver na aba *Minhas Atividades*  
**Então** poderá ser um *Rascunho*  
**Ou** poderá estar *Pronta para aplicar*  
**Quando** a atividade estiver na aba *Atividades Aplicadas Online*  
**Então** poderá estar *Em Andamento*  
**Ou** poderá estar *Concluída*

#### Corrigindo atividades entregues

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** clico numa atividade da aba *Atividades Aplicadas Online*  
**Então** devo ter acesso às atividades de cada um dos alunos marcados para a atividade aplicada  
**Quando** a atividade estiver marcada como *Entregue*  
**Então** devo ser capaz de selecioná-la  
**E** corrigir as questões respondidas pelo aluno, podendo também criar observações ao lado de cada resposta  
**E** por fim devo ser capaz de salvar, clicando sobre o botão *Salvar*, disponibilizando para os alunos a atividade devidamente corrigida  

___

### Central da Turma  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Central da Turma*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ter acesso aos alunos e professores da turma a qual estou contextualizado  
**Quando** clicar na miniatura do perfil de algum aluno  
**Então** devo ser capaz de ver detalhes daquele aluno, como anotações gerais feitos por mim e outros professores da turma, além das suas faltas  

___

### Arquivos    

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Arquivos*](https://se.ftd.com.br/arquivos) no menu  
**E** clico no botão *Novo* escolhendo a opção *Upload de arquivo*  
**Então** o sistema deve apresentar um modal onde devo selecionar arquivos do computador e clicar no botão *Upload*  
**Quando** clicar sobre um arquivo já salvo  
**Então** devo ser capaz de visualizar o arquivo selecionado  
**Quando** clicar sobre as "reticências" presentes ao lado de algum arquivo já salvo  
**Então** devo ser capaz de visualizar, excluir, baixar, renomear, compartilhar ou mover o arquivo  

#### Compartilhando arquivos

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Arquivos*](https://se.ftd.com.br/arquivos) no menu  
**E** clico sobre a opção *Compartilhar* contida nas reticências ao lado de qualquer arquivo  
**Então** devo ser capaz de compartilhar o arquivo com qualquer turma a qual esteja designado ou com qualquer aluno de uma dessas turmas  

#### Criando pastas

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Arquivos*](https://se.ftd.com.br/arquivos) no menu  
**E** clico no botão *Novo* escolhendo a opção *Nova Pasta*  
**Então** devo ser capaz de criar uma nova pasta nomeando-a e clicando no botão *Criar*  

___

### Acervo de Links  

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Acervo de links*](https://se.ftd.com.br/links) no menu  
**Então** terei acesso a um acervo de links, com curadoria da FTD, sobre assuntos ligados a pedagogia, conhecimentos acadêmicos e conhecimentos gerais  
**E** ao acessar um desses itens, o sistema deve apresentar o ambiente do encurtador de links da FTD que me assegurará que o endereço selecionado continua ativo e acessível  

___  

### Formação Continuada

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Formação Continuada*](https://se.ftd.com.br/formacao-continuada) no menu  
**Então** devo ser capaz de visualizar e acessar cursos disponíveis  
**E** ler o regulamento clicando no botão *Regulamento*

#### Completando cursos

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Formação Continuada*](https://se.ftd.com.br/formacao-continuada) no menu  
**E** acesso um dos cursos disponíveis  
**Então** devo ser capaz de visualizar informações sobre o curso, como carga horária, número de avaliações, número de textos, número de vídeos e número de orientações  
**E** também devo ser capaz de iniciar o curso caso não tenha iniciado  
**Ou** desistir caso já tenha iniciado  
**Quando** inicio um curso não iniciado  
**Então** devo ser capaz de acessar os módulos do curso e seus respectivos conteúdos, dispostos nas abas: *Assista*, *Leia*, *Orientação* e *Avaliação*  
**Quando** acesso a aba *Assista*  
**Então** devo ser capaz de assistir o vídeo referente ao módulo acessado clicando no botão *play*  
**E** depois de abrir o vídeo, a aba *Assista* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Leia*  
**Então** devo ser capaz de ler o texto referente ao módulo acessado clicando no card com o título do referido texto  
**E** depois de abrir o texto, a aba *Leia* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Orientação*  
**Então** devo ser capaz de ler a orientação referente ao módulo acessado clicando no card com o título do referido texto  
**E** depois de abrir o texto de orientação, a aba *Orientação* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Avaliação*  
**Então** devo ser capaz de responder as questões propostas  
**E** depois de responder a todas as perguntas, devo ser capaz de entregar a avaliação clicando no botão *Entregar avaliação*  
**Quando** clico no botão *Entregar avaliação*  
**Então** o sistema deve exibir um aviso notificando se foi ou não aprovado na avaliação  
**E** depois da avaliação ser entregue devo ser capaz de acessar minhas respostas e conferir as correções  

#### Tirando dúvidas  

**Dado que** sou um professor logado com sucesso  
**E** já iniciei um curso na seção [*Formação continuada*](https://se.ftd.com.br/formacao-continuada)  
**Quando** acesso um curso já iniciado  
**E** clico no botão *Tirar dúvidas*  
**Então** o sistema deve apresentar um modal onde posso enviar mensagens para a central de atendimento FTD a fim de esclarecer eventuais dúvidas  

___  

### Leitores FTD

**Dado que** sou um professor logado com sucesso  
**Quando** acesso a opção [*Leitores*](http://leitores.ftd.com.br/) no menu  
**Então** o sistema deverá abrir uma nova aba no navegador dando acesso a uma página simples e informativa sobre como baixar o Leitor FTD para
as mais diferentes plataformas  

___

### Logout

**Dado que** sou um professor logado com sucesso  
**Quando** estiver em qualquer seção ou módulo do portal  
**Então** devo ser capaz de deslogar clicando no ícone de perfil, e depois clicando em *Sair*  
**E** o sistema deve apresentar novamente a tela de Login  

___

## Caminho do Diretor

### Login

**Dado que** sou um usuário cadastrado  
**Quando** acesso a tela de login  
**E** insiro as credenciais corretas  
**Então** devo ser capaz de acessar a home do sistema  

___

### Home page

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a home page  
**Então** devo ser capaz de ver e acessar os destaques dinâmicos a seguir: *Eventos e feriados*  
**E** em Eventos e Feriados devem ser apresentadas datas especiais do mês corrente  

___

### Menus  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso qualquer página do sistema com exceção de [*Leitores*](http://leitores.ftd.com.br/)  
**Então** devo ser capaz de acessar o menu do lado esquerdo da tela com as opções: *Início*, *Publicações e recursos*, *Planejador de Aulas*, *Aula*, *Atividades*, *Central da turma* e *Acervo de links*  

___  

### Publicações e recursos  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**Então** devo ser capaz de acessar as abas: *Publicações* e *Recursos FTD* além de seus respectivos conteúdos  
**E** também devo ser capaz de usar, em ambas as abas, a busca de itens por nome, filtro de disciplina, ordem alfabética, favoritos e controle de itens exibidos por página  

#### Vincular publicações  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Publicações*  
**Então** devo ser capaz de visualizar e selecionar múltiplas publicações  
**E** vinculá-las ao leitor clicando no botão *vincular ao Leitor FTD*  

#### Filtrar recursos  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Recursos FTD*  
**Então** devo ser capaz de visualizar e filtrar os OEDs listados por tipo, sendo todos: áudio, vídeo e multimídia (peças
interativas em html5)  
**E** também devo ser capaz de favoritar múltiplos OEDs  

___

### Planejador de Aulas  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção *Planejador de Aulas* no menu  
**Então** devo ser capaz de visualizar e baixar os mapas didáticos aplicados para a disciplina e ano letivo relacionados no header  
**E** também devo ser capaz de visualizar as sequências didáticas que compõe o mapa acessado  

___  

### Aula  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**Então** devo ser capaz de acessar um calendário de visualização mensal ou semanal com todas as aulas, aulas extras e eventos associados ao período exibido e à turma selecionada no header  

#### Visualizando uma aula  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula exibida no calendário  
**Então** devo ser capaz de acessar todo o conteúdo inserido pelo Professor tal como: recursos da FTD (OED’s), descrições com objetivo da aula,conteúdos, materiais, avaliação, habilidades, Para saber + e/ou desenvolvimento  
**E** também devo ser capaz de ver o status no qual se encontram as aulas: : Aula publicada (verde), Aula não publicada (amarelo), Aula Extra (Um pequeno “E” no canto superior esquerdo do evento), Evento da Escola (azul), Evento da Turma (rosa)  

___

### Atividades  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ser capaz de visualizar atividades em divididas em duas abas: *Atividades Aplicadas Online*, *Atividades aplicadas impressas*  
**E** para a aba *Atividades Aplicadas Online*, deverão constar três status possíveis  
**Quando** a atividade estiver na aba *Atividades Aplicadas Online*  
**Então** poderá estar *Em Andamento*  
**Ou** poderá estar *Concluída*  
**Ou** poderá estar *Aguardando início*  

___

### Central da Turma  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Central da Turma*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ter acesso aos alunos e professores da turma a qual estou contextualizado  
**Quando** clicar na miniatura do perfil de algum aluno  
**Então** devo ser capaz de ver detalhes daquele aluno, como anotações gerais feitos por professores da turma, além das suas faltas  

___

### Acervo de Links  

**Dado que** sou um diretor logado com sucesso  
**Quando** acesso a opção [*Acervo de links*](https://se.ftd.com.br/links) no menu  
**Então** terei acesso a um acervo de links, com curadoria da FTD, sobre assuntos ligados a pedagogia, conhecimentos acadêmicos e conhecimentos gerais  
**E** ao acessar um desses itens, o sistema deve apresentar o ambiente do encurtador de links da FTD que me assegurará que o endereço selecionado continua ativo e acessível  

___  

### Logout

**Dado que** sou um diretor logado com sucesso  
**Quando** estiver em qualquer seção ou módulo do portal  
**Então** devo ser capaz de deslogar clicando no ícone de perfil, e depois clicando em *Sair*  
**E** o sistema deve apresentar novamente a tela de Login  

___

## Caminho do Coordenador  

### Login

**Dado que** sou um usuário cadastrado  
**E** devidamente ensalado  
**Quando** acesso a tela de login  
**E** insiro as credenciais corretas  
**Então** devo ser capaz de acessar a home do sistema  

___

### Home page

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a home page  
**Então** devo ser capaz de ver e acessar os destaques dinâmicos a seguir: *Atividade online de hoje*, *Aulas de hoje* e *Eventos e feriados*  
**E** em Atividades Online de Hoje deve mostrar a lista das atividades, desde que vinculadas a turma que está selecionada no Header e marcadas para iniciar no dia do acesso  
**Ou** em caso de não haver Atividades pro dia, deverá aparecer apenas um botão [Ir para Atividades](https://se.ftd.com.br/atividades)  
**E** em Aulas de Hoje deve apresentar uma lista com todas aulas e/ou eventos, desde que vinculadas a turma que está selecionada no Header e marcados para o dia do acesso  
**Ou**  em caso de não haver Aulas/Eventos pro dia, deverá aparecer apenas um botão [Ir para Aulas](https://se.ftd.com.br/aula)   
**E** em Eventos e Feriados devem ser apresentadas datas especiais do mês corrente  

___

### Menus  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso qualquer página do sistema com exceção de [*Leitores*](http://leitores.ftd.com.br/)  
**Então** devo ser capaz de acessar o menu do lado esquerdo da tela com as opções: *Início*, *Publicações e recursos*, *Planejador de Aulas*, *Aula*, *Atividades*, *Central da turma*, *Arquivos*, *Acervo de links*, *Formação Continuada* e *Leitores*  

___  

### Publicações e recursos  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**Então** devo ser capaz de acessar as abas: *Publicações* e *Recursos FTD* além de seus respectivos conteúdos  
**E** também devo ser capaz de usar, em ambas as abas, a busca de itens por nome, filtro de disciplina, ordem alfabética, favoritos e controle de itens exibidos por página  

#### Vincular publicações  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Publicações*  
**Então** devo ser capaz de visualizar e selecionar múltiplas publicações  
**E** vinculá-las ao leitor clicando no botão *vincular ao Leitor FTD*  

#### Filtrar recursos  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Publicações e recursos*](https://se.ftd.com.br/publicacoes-recursos) no menu  
**E** clico na aba *Recursos FTD*  
**Então** devo ser capaz de visualizar e filtrar os OEDs listados por tipo, sendo todos: áudio, vídeo e multimídia (peças
interativas em html5)  
**E** também devo ser capaz de favoritar múltiplos OEDs  

___

### Planejador de Aulas  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção *Planejador de Aulas* no menu  
**Então** devo ser capaz de visualizar e baixar os mapas didáticos aplicados para a disciplina e ano letivo relacionados no header  
**E** também devo ser capaz de visualizar as sequências didáticas que compõe o mapa acessado  

___  

### Aula  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**Então** devo ser capaz de acessar um calendário de visualização mensal ou semanal com todas as aulas, aulas extras e eventos associados ao período exibido e à turma selecionada no header  

#### Visualizando uma aula  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula exibida no calendário  
**Então** devo ser capaz de acessar todo o conteúdo inserido pelo Professor tal como: recursos da FTD (OED’s), descrições com objetivo da aula,conteúdos, materiais, avaliação, habilidades, Para saber + e/ou desenvolvimento  
**E** também devo ser capaz de ver o status no qual se encontram as aulas: : Aula publicada (verde), Aula não publicada (amarelo), Aula Extra (Um pequeno “E” no canto superior esquerdo do evento), Evento da Escola (azul), Evento da Turma (rosa)  

#### Criando eventos ou aulas extras  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer dia exibido no calendário  
**Então** devo ser capaz de configurar as informações do evento como data, hora de início e fim, além de escolher entre os tipos *Aula Extra* ou *Evento*  
**Quando** a opção escolhida for *Evento*  
**Então** o sistema deve solicitar, além das informações mencionadas, título e descrição  
**Quando** a opção escolhida for *Aula Extra*  
**Então** devo indicar, além das informações mencionadas, a sequência didática que desejo associar a essa aula  

#### Publicando, encerrando ou excluindo aulas extras ou eventos

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Aula*](https://se.ftd.com.br/aula) no menu  
**E** clico em qualquer aula não publicada exibida no calendário  
**Então** devo ser capaz de publicar a aula clicando em *Publicar Aula*  
**E** o status dessa aula deve mudar de aula não publicada para aula publicada  
**Quando** clico em qualquer aula publicada exibida no calendário  
**Então** devo ser capaz de encerrar a aula clicando em *Encerrar Aula*  
**Quando** clico em qualquer evento exibido no calendário  
**Então** devo ser capaz de excluí-lo clicando no símbolo de lixeira  

___

### Atividades  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** clico no botão criar atividade  
**Então** devo ser capaz de criar uma atividade, optando por online ou impressa  
**E** indicando um nome e descrição para a mesma  
**E** adiante, clicando em avançar devo ser capaz de selecionar questões curadas pela FTD ou de autoria própria, para que façam parte da atividade  
**E** as questões selecionadas devem ser exibidas num "carrinho de compra" ao lado direito da tela, podendo ser rearranjadas ou removidas  
**E** também devo ser capaz de determinar notas para cada questão selecionada.  

#### Criando questões próprias  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** estiver selecionando questões para compor uma atividade  
**Então** devo ser capaz de criar questões próprias clicando no ícone + no fim da listagem no bloco "Insira questões do seu acervo pessoal"  
**E** no modal que se abrir, devo indicar informações de classificação como tags, nível de ensino e disciplina além de determinar o tipo da questão  
**E** desenvolver um enunciado, usando texto e/ou imagem  
**E** desenvolver uma resolução, usando texto e /ou imagem  
**E** essa questão deve estar disponível apenas no meu acervo , apenas para acesso pessoal, junto a lista de questões curadas pela FTD  

#### Aplicando atividades  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** acesso uma atividade marcada como *Pronta para Aplicar*  
**Então** devo ser capaz de definir uma turma para qual quero aplicar a atividade  
**E** escolher o horário de início e fim da mesma assim como data  
**E** também devo ser capaz de escolher se a prova deve ter tempo controlado e se deve ser aplicada para toda a turma  
**Quando** o tempo for controlado  
**Então** devo ser capaz de definir quanto tempo os alunos terão para realizar a atividade  
**Quando** a atividade for aplicada apenas para alguns alunos  
**Então** devo ser capaz de selecionar os alunos para os quais a atividade estará disponível  
**Ou** na lista de atividades criadas  
**Quando** acesso uma atividade marcada como *Rascunho*  
**Então** devo ser capaz de editar as questões e nota da atividade  
**E** aplicá-la seguindo o processo descrito para atividades marcadas como *Pronta para aplicar*  

#### Visualizando atividades  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ser capaz de visualizar atividades em divididas em três abas: *Minhas Atividades*, *Atividades Aplicadas Online*, *Atividades aplicadas impressas*  
**E** para cada uma das abas, com exceção de *Atividades Aplicadas Impressas*, deverão constar dois status possíveis  
**Quando** a atividade estiver na aba *Minhas Atividades*  
**Então** poderá ser um *Rascunho*  
**Ou** poderá estar *Pronta para aplicar*  
**Quando** a atividade estiver na aba *Atividades Aplicadas Online*  
**Então** poderá estar *Em Andamento*  
**Ou** poderá estar *Concluída*

#### Corrigindo atividades entregues

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Atividades*](https://se.ftd.com.br/atividades) no menu  
**E** clico numa atividade da aba *Atividades Aplicadas Online*  
**Então** devo ter acesso às atividades de cada um dos alunos marcados para a atividade aplicada  
**Quando** a atividade estiver marcada como *Entregue*  
**Então** devo ser capaz de selecioná-la  
**E** corrigir as questões respondidas pelo aluno, podendo também criar observações ao lado de cada resposta  
**E** por fim devo ser capaz de salvar, clicando sobre o botão *Salvar*, disponibilizando para os alunos a atividade devidamente corrigida  

___

### Central da Turma  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Central da Turma*](https://se.ftd.com.br/atividades) no menu  
**Então** devo ter acesso aos alunos e professores da turma a qual estou contextualizado  
**Quando** clicar na miniatura do perfil de algum aluno  
**Então** devo ser capaz de ver detalhes daquele aluno, como anotações gerais feitas por mim e outros professores da turma, além das suas faltas  

___

### Arquivos    

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Arquivos*](https://se.ftd.com.br/arquivos) no menu  
**Então** devo ser capaz de visualizar ou baixar arquivos compartilhados com a turma destacada no header  

___

### Acervo de Links  

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Acervo de links*](https://se.ftd.com.br/links) no menu  
**Então** terei acesso a um acervo de links, com curadoria da FTD, sobre assuntos ligados a pedagogia, conhecimentos acadêmicos e conhecimentos gerais  
**E** ao acessar um desses itens, o sistema deve apresentar o ambiente do encurtador de links da FTD que me assegurará que o endereço selecionado continua ativo e acessível  

___  

### Formação Continuada

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Formação Continuada*](https://se.ftd.com.br/formacao-continuada) no menu  
**Então** devo ser capaz de visualizar e acessar cursos disponíveis  
**E** ler o regulamento clicando no botão *Regulamento*

#### Completando cursos

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Formação Continuada*](https://se.ftd.com.br/formacao-continuada) no menu  
**E** acesso um dos cursos disponíveis  
**Então** devo ser capaz de visualizar informações sobre o curso, como carga horária, número de avaliações, número de textos, número de vídeos e número de orientações  
**E** também devo ser capaz de iniciar o curso caso não tenha iniciado  
**Ou** desistir caso já tenha iniciado  
**Quando** inicio um curso não iniciado  
**Então** devo ser capaz de acessar os módulos do curso e seus respectivos conteúdos, dispostos nas abas: *Assista*, *Leia*, *Orientação* e *Avaliação*  
**Quando** acesso a aba *Assista*  
**Então** devo ser capaz de assistir o vídeo referente ao módulo acessado clicando no botão *play*  
**E** depois de abrir o vídeo, a aba *Assista* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Leia*  
**Então** devo ser capaz de ler o texto referente ao módulo acessado clicando no card com o título do referido texto  
**E** depois de abrir o texto, a aba *Leia* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Orientação*  
**Então** devo ser capaz de ler a orientação referente ao módulo acessado clicando no card com o título do referido texto  
**E** depois de abrir o texto de orientação, a aba *Orientação* deve mudar a marcação para indicar que já foi acessada  
**Quando** acesso a aba *Avaliação*  
**Então** devo ser capaz de responder as questões propostas  
**E** depois de responder a todas as perguntas, devo ser capaz de entregar a avaliação clicando no botão *Entregar avaliação*  
**Quando** clico no botão *Entregar avaliação*  
**Então** o sistema deve exibir um aviso notificando se foi ou não aprovado na avaliação  
**E** depois da avaliação ser entregue devo ser capaz de acessar minhas respostas e conferir as correções  

#### Tirando dúvidas  

**Dado que** sou um coordenador logado com sucesso  
**E** já iniciei um curso na seção [*Formação continuada*](https://se.ftd.com.br/formacao-continuada)  
**Quando** acesso um curso já iniciado  
**E** clico no botão *Tirar dúvidas*  
**Então** o sistema deve apresentar um modal onde posso enviar mensagens para a central de atendimento FTD a fim de esclarecer eventuais dúvidas  

___  

### Leitores FTD

**Dado que** sou um coordenador logado com sucesso  
**Quando** acesso a opção [*Leitores*](http://leitores.ftd.com.br/) no menu  
**Então** o sistema deverá abrir uma nova aba no navegador dando acesso a uma página simples e informativa sobre como baixar o Leitor FTD para
as mais diferentes plataformas  

___

### Logout

**Dado que** sou um coordenador logado com sucesso  
**Quando** estiver em qualquer seção ou módulo do portal  
**Então** devo ser capaz de deslogar clicando no ícone de perfil, e depois clicando em *Sair*  
**E** o sistema deve apresentar novamente a tela de Login  

___