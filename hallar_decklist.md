## Hallar decklist  

### Card draw (10)

lifecrafter's bestiary  
colossal majesty  
harmonize  
snake umbra  
zendikar ressurgent  
soul's might  
abzan beastmaster  
guardian project  
beast whisperer  
runic armassaur
soul of the harvest  
fruit of the first tree  
triumph of ferocity  
loyal guardian  


### Ramp (10)

Zendikar resurgent  
kodamas reach  
cultivate  
arbor elf  
elfhame druid  
llanowar elf  
wood elves  
zhur-ta druid  
generator servant  
sol ring  
savage ventmaw  
everflowing chalice  
rishkar, peema renegade  
hazorett's monumment
grow from the ashes  
shefet monitor  


### Spot removal (5)

mold shambler  
savage stomping  
return to earth  
shivan fire  
territorial allosaurus

### board wipe (5)

hour of devastation  
blasphemous act  
Whiptongue Hydra  
Ashling the Pilgrim  
Chain Reaction

### Standalone (x)

gravity well  
Domri, Chaos Bringer  
Chandra, Bold Pyromancer  


### enabler (x)  

Rhythm of the Wild
swiftfoot boots  
lighting greaves  
insult // injury  
vigor  
temur sabertooth  